# Package

version       = "0.0.1"
author        = "Michael Riley"
description   = "A project to transcribe and upload vimwiki files to Gitlab"
license       = "MIT"

bin = @["vimwiki_gitlab", "wiki_parse"]
srcDir = "src"
binDir = "bin"
skipExt = @["nim"]

# Dependencies

requires "nim >= 0.17.2"
