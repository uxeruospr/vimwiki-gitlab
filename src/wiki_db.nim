import tables, db_postgres, parsecfg, strutils, times, SecureHash
import wiki_type

var config = loadConfig("config.ini")

let db_srv = config["database"]["server"]
let db_usr = config["database"]["user"]
let db_pas = config["database"]["password"]
let db_nam = config["database"]["name"]

proc getWikiDbId*(title: string): int =
  let db = open(db_srv, db_usr, db_pas, db_nam)

  let sql = sql("""
    SELECT id 
      FROM page
     WHERE title = ? 
  """)

  let id = getValue(db, sql, title)

  db.close()

  if len(id) > 0:
    return id.parseInt()
  
  else:
    return 0
  
proc getWikiDbParsed*(id: int): string =
  let db = open(db_srv, db_usr, db_pas, db_nam)
  let sql = sql("""
    SELECT parsed
      FROM page
     WHERE id = ?
  """)

  let parsed = getValue(db, sql, id) 

  db.close()

  return parsed

proc updateWikiPageMetadata*(page: WikiPage): bool =
  let db = open(db_srv, db_usr, db_pas, db_nam)
  let sql = sql("""
    UPDATE page
       SET
           title = $1,
           path = $2,
           word_count = $3,
           hash = $4,
           parsed = NOW(),
           modified = $5,
           created = $6
     WHERE id = $7
  """)

  let stmt = prepare(db, "metadataUpdate", sql, 7)

  let res = tryExec(db, stmt, page.title, page.path, page.word_count, $page.hash, page.modified, page.created, page.id) 

  db.close()

  return res

proc insertWikiPageMetadata*(page: WikiPage): bool =
  let db = open(db_srv, db_usr, db_pas, db_nam)
  let sql = sql("""
    INSERT INTO page (title, path, word_count, hash, parsed, modified, created)
         VALUES ($1, $2, $3, $4, NOW(), $5, $6)
  """)

  let stmt = prepare(db, "metadataInsert", sql, 5)

  let res = tryExec(db, stmt, page.title, page.path, page.word_count, $page.hash, page.modified, page.created) 

  db.close()

  return res

proc getWikiDbHash*(id: int): SecureHash =
  let db = open(db_srv, db_usr, db_pas, db_nam)
  let sql = sql("""
    SELECT hash 
      FROM page
     WHERE id = ?
  """)

  let hash: SecureHash = parseSecureHash(getValue(db, sql, id))

  db.close()

  return hash
