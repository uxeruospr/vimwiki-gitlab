import securehash

type 
  WikiPage* = object
    id*: int
    title*: string
    path*: string
    wordCount*: int
    hash*: SecureHash
    tags*: seq[string] 
    links*: seq[string]
    created*: string
    modified*: string
