import httpclient, json, parsecfg, tables, system, net, strutils

type
  HttpReqParams = object
    url: string
    token: string
    reqMethod: HttpMethod 
    body: JsonNode
    exStatus: string
    parseResponse: bool

var config = loadConfig("config.ini")

let project = config["gitlab"]["project"]
let url = config["gitlab"]["url"] & "/api/v4/projects/" & project & "/wikis"
let token = config["gitlab"]["token"]


proc httpReq(params: HttpReqParams): JsonNode = 

  var checkStatus = "200"

  if not isNil(params.exStatus):
    checkStatus = params.exStatus

  let client = newHttpClient()
  var reqBody = ""

  client.headers = newHttpHeaders({"PRIVATE-TOKEN": params.token})
  
  if not isNil(params.body):
    reqBody = $params.body
    client.headers.add("Content-Type", "application/json")
  else:
    reqBody = "" 

  try:
    let response = client.request(params.url, params.reqMethod, reqBody)

    if not contains(response.status, checkStatus):
      echo "HTTP Status Error: " & response.status
      client.close()

    client.close()

    echo response.status

    if params.parseResponse:
      return parseJson(response.body)
    else:
      return %*"[{}]" # Empty JSON object

  except TimeoutError:
    echo getCurrentExceptionMsg()


proc listWikis(url: string, token: string): JsonNode =
  
  var reqParams = HttpReqParams(
    url: url, 
    token: token, 
    reqMethod: HttpMethod.HttpGet)

  return httpReq(reqParams)


proc addWiki(url: string, token: string, title: string, content: string, 
             format: string): void =

  let reqBody = %*{
    "title": title,
    "content": content,
    "format": format
  }

  var reqParams = HttpReqParams(url: url, token: token, 
                                reqMethod: HttpMethod.HttpPost, body: reqBody, 
                                exStatus: "201")

  discard(httpReq(reqParams))


proc getWiki(url: string, token: string, slug: string): JsonNode =

  let reqUrl = url & "/" & slug 
  var reqParams = HttpReqParams(url: reqUrl, token: token, 
                                reqMethod: HttpMethod.HttpGet)

  return httpReq(reqParams)


proc delWiki(url: string, token: string, slug: string): void =

  let reqUrl = url & "/" & slug 
  var reqParams = HttpReqParams(url: reqUrl, token: token, 
                                reqMethod: HttpMethod.HttpDelete, 
                                exStatus: "204")

  discard(httpReq(reqParams))


proc editWiki(url: string, token: string, slug: string, title: string, 
              content: string, format: string, newSlug: string): JsonNode =

  let reqUrl = url & "/" & title
  let reqBody = %*{
    "title": title,
    "content": content,
    "format": format,
    "slug": newSlug
  }

  var reqParams = HttpReqParams(url: reqUrl, token: token, 
                                reqMethod: HttpMethod.HttpPut, body: reqBody)

  discard(httpReq(reqParams))

# List
# discard(listWikis(url, token))

# Get
# discard(getWiki(url, token, "finnboy"))

# Edit
# discard(editWiki(url, token, "test", "test", "The last one who went there turned into a crow", "markdown", "test"))

# Delete
# delWiki(url, token, "test5")

# Add
# addWiki(url, token, "Crowpie", "Is a swell guy", "markdown")
