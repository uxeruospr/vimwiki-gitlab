# TODO: use date types instead of strings?

import system, parsecfg, tables, streams, os, strutils, times, nre, osproc, securehash
import wiki_db, wiki_type
import options

var config = loadConfig("config.ini")

let wiki_path = config["vimwiki"]["path"]

iterator wikiFiles(): string =
  for file in walkFiles(wiki_path & "*.wiki"):
    yield file

# Convert the options type to get the value instead
# This is stolen from some nim unit tests and should be revisted
# TODO: make a more purpose build converter
converter option2val*[T](val: Option[T]): T =
  return val.get()

proc wikiCount(): int =
  var total: int = 0
  for file in wikiFiles():
    total += 1

  return total

proc getWikiPageFilename(path: string): string =
  let path_seq = path.split("/")
  return path_seq[high(path_seq)]

proc getWikiPageTitle(path: string): string =
  let filename = getWikiPageFilename(path)
  let title = filename.findAll(re"^.*(?=.wiki)")

  return title[0]

proc getWikiPageWordCount(path: string): int =
  var count: int = 0
  for line in lines(path):
    count += len(line.splitWhitespace())

  return count

proc getWikiPageModified(path: string): string =
  return $(getLastModificationTime(path))

proc getWikiPageCreated(path: string): string =

  # Get the date of the commit where the file was added
  let cmd = "cd " & quoteShellPosix(wiki_path) & " && " & 
    "git log --pretty=\"format:%cI\" --diff-filter=A " & 
    quoteShellPosix(path)

  return execProcess(cmd).strip()

proc getWikiPageTags(path: string): seq[string] =
  for line in lines(path):
    if line.startsWith(":"):
      let tags = line.split(":")
      return tags[1..high(tags) - 1]

proc getWikiPageLinks(path: string): seq[string] =
  # TODO: handle more than one link on a single line
  var links = @[""]
  for line in lines(path):
    try:
      let link = line.find(re"(\[\[)(#)?(?<l>[a-zA-Z _-]*)")
      links.add(link.captures.toTable["l"])

    except UnpackError:
      # No match on line
      continue

  return links

proc hashWikiPage(path: string): SecureHash =
  let file = readFile(path)
  var file_hash: SecureHash = secureHashFile(path)

  return file_hash

proc getWikiPage(path: string, id: int, hash: SecureHash): WikiPage =
  let title = getWikiPageTitle(path)

  if id == 0:
    let id = getWikiDbId(title)

  let parsed = getWikiDbParsed(id)
  let count = getWikiPageWordCount(path)
  let created = getWikiPageCreated(path)
  let modified = getWikiPageModified(path)
  let tags = getWikiPageTags(path) 
  let links = getWikiPageLinks(path)

  var page = WikiPage(
    id: id,
    title: title,
    path: path,
    wordCount: count,
    hash: hash,
    tags: tags,
    links: links,
    created: created,
    modified: modified
  )

  return page 


# ----------------------------

proc parseWikiPageMetadata(): void =
  for file in wikiFiles():
    let page_title = getWikiPageTitle(file)
    let page_id = getWikiDbId(page_title)
    let page_hash = hashWikiPage(file)

    if page_id == 0:
      # We have a new page
      let page = getWikiPage(file, page_id, page_hash)
      echo("Inserting page: " & page.title)
      discard insertWikiPageMetadata(page)

    else:
      # Check if anything has changed
      let page_db_hash = getWikiDbHash(page_id)
      if page_db_hash != page_hash:
        let page = getWikiPage(file, page_id, page_hash)
        echo("Updating page: " & page.title)
        discard updateWikiPageMetadata(page)

      else:
        echo("No changes: " & page_title)

parseWikiPageMetadata()
