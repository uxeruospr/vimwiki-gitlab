DROP TABLE IF EXISTS page_tag;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS page_link;
DROP TABLE IF EXISTS page;

CREATE TABLE page (
	id serial PRIMARY KEY,
	title varchar(255),
	path varchar(255),
	word_count integer,
	parsed timestamp DEFAULT CURRENT_TIMESTAMP,
	modified timestamp,
	created timestamp DEFAULT CURRENT_TIMESTAMP,
	deleted timestamp 
);

CREATE TABLE page_link (
	id serial PRIMARY KEY,
	link_from integer,
	link_to integer,
	created timestamp DEFAULT CURRENT_TIMESTAMP,
	deleted timestamp,

	FOREIGN KEY (link_from) REFERENCES page (id),
	FOREIGN KEY (link_to) REFERENCES page (id)
);

CREATE TABLE tag (
	id serial PRIMARY KEY,
	name varchar(20),
	created timestamp DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE page_tag (
	id serial PRIMARY KEY,
	page_id integer,
	tag_id integer,
	created timestamp DEFAULT CURRENT_TIMESTAMP,
	deleted timestamp,

	FOREIGN KEY (page_id) REFERENCES page (id),
	FOREIGN KEY (tag_id) REFERENCES tag (id)
);

